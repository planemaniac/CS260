import Vue from 'vue'
import Router from 'vue-router'
import HomePage from '@/components/HomePage'
import SearchResults from '@/components/SearchResults'
import HashTag from '@/components/HashTag'
import UserPage from '@/components/UserPage'

Vue.use(Router)

export default new Router({
	routes: [
		{
			path: '/',
			name: 'HomePage',
			component: HomePage,
		},
		{
			path: '/search',
			name: 'Search Results',
			component: SearchResults,
		},
		{
			path: '/hashtag/:hashtag',
			name: 'HashTag',
			component: HashTag,
		},
		{
			path: '/user/:userID',
			name: 'UserPage',
			component: UserPage,
		},
	]
})
