const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static('dist'));

// User API
const users = new Set();
const userStatus = {};
const USER_TIMEOUT = 60;
const USER_REMOVE = 60 * 5;
const USER_STATUS = {
    OFFLINE: 0,
    ONLINE: 1,
}

setInterval( () => {
    let timeout = Date.now() - USER_TIMEOUT * 1000;
    let remove = Date.now() - USER_REMOVE * 1000;
    let curUsers = Array.from( users );

    for ( let user of curUsers ) {
        let lastSeen = userStatus[user].lastSeen;
        if ( lastSeen < remove ) {
            users.delete( user );
        } else if ( lastSeen < timeout ) {
            userStatus[user].status = USER_STATUS.OFFLINE;
        } else {
            userStatus[user].status = USER_STATUS.ONLINE;
        }
    }
}, USER_TIMEOUT * 1000 / 3 );

// Query users with their statuses
app.get( '/api/users', ( req, res ) => {
    res.send( Array.from( users ).map( user => {
        return {
            name: user,
            status: userStatus[user].status,
        }
    } ) );
} );

// Query if username is registered
app.get( '/api/users/:user', ( req, res ) => {
    let user = req.params.user;
    res.send( users.has( user ) );
} );

// Register username
app.post( '/api/users/register', ( req, res ) => {
    let newUser = req.body.username;

    users.add( newUser );
    userStatus[newUser] = {
        status: USER_STATUS.ONLINE,
        lastSeen: Date.now(),
    }
    res.sendStatus( 200 );
} );


// Message api
const MESSAGE_DELETE = 5 * 60;
let messages = [];

setInterval( () => {
    let timeout = Date.now() - MESSAGE_DELETE * 1000;
    let startSize = messages.length;

    if ( !startSize ) return;

    messages = messages.filter( message => {
        return message.timestamp > timeout;
    } );
    let endSize = messages.length;

    console.log( `Pruning messages: ${startSize - endSize} message(s) deleted` );
}, MESSAGE_DELETE * 1000 / 3 );

app.get( '/api/messages', ( req, res ) => {
    res.send( messages );
} );

app.post( '/api/messages', ( req, res ) => {
    let user = req.body.user;
    let message = req.body.message;

    if ( !user || !message ) {
        res.sendStatus( 400 );
        return;
    }

    userStatus[user].lastSeen = Date.now();

    messages.push( {
        name: user,
        message: message,
        timestamp: Date.now(),
    } );
    res.send( messages );
} );

app.delete( '/api/messages/:id', ( req, res ) => {
    let id = parseInt( req.params.id );
    let { index, user } = req.body;
    index = parseInt( index );

    let deletedMessage = `>>message deleted by ${user}!<<`;

    if ( messages[index].timestamp === id ) {
        messages[index].message = deletedMessage;
        res.send( messages );
        return;
    }

    for ( let message of messages ) {
        if ( message.timestamp !== id ) continue;

        message.message = deletedMessage;
    }

    res.send( messages );
    return;
} );


app.listen(3001, () => console.log('Server listening on port 3001!'));