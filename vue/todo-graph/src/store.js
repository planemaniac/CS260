import Vue from 'vue';
import Vuex from 'vuex';

import axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        items: [],
        show: 'all',
    },
    getters: {
        items: state => state.items,
        show: state => state.show,
    },
    mutations: {
        setItems( store, items ) {
            store.items = items;
        },
        setShow( store, show ) {
            store.show = show;
        },
    },
    actions: {
        getItems(context) {
            console.log("getting items");
            axios.get("/api/items").then(response => {
                context.commit('setItems', response.data);
                return true;
            }).catch(err => {
            });
        },
        addItem(context, item) {
            axios.post("/api/items", item).then(response => {
                return context.dispatch('getItems');
            }).catch(err => {
            });
        },
        updateItem(context, item) {
            axios.put("/api/items/" + item.id, item).then(response => {
                return true;
            }).catch(err => {
            });
        },
        deleteItem(context, item) {
            axios.delete("/api/items/" + item.id).then(response => {
                return context.dispatch('getItems');
            }).catch(err => {
            });
        },
        updateShow(context, show) {
            context.commit( 'setShow', show );
        },
    }
});