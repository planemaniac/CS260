import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Login from '@/components/Login'
import Complaints from '@/components/Complaints'

Vue.use(Router)

import store from '../store';

const router = new Router({
	routes: [
		{
			path: '/',
			name: 'Home',
			component: Home,
			meta: {
				protected: true,
			},
		},
		{
			path: '/complaints',
			name: 'Complaints',
			component: Complaints,
			meta: {
				protected: true,
			},
		},
		{
			path: '/login',
			name: 'Login',
			component: Login,
		},
		{
			path: '/register',
			name: 'Register',
			component: Login,
		},
	],
});

router.beforeEach( (to, from, next) => {
	let route = null;
	to.matched.some( record => {
		route = record;
	} )
	if ( route.meta.protected && !store.getters.loggedIn ) {
		store.commit( 'setRedirect', route.name );
		next( {
			name: 'Login',
		} );
		return;
	}

	next();
} );

export default router;
