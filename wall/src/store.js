import Vue from 'vue';
import Vuex from 'vuex';

import axios from 'axios';

Vue.use(Vuex);

const store = new Vuex.Store({
	state: {
		user: null,
		token: null,
		errorString: '',
		redirect: null,
	},
	getters: {
		loggedIn: state => state.user !== null && state.token !== null,
		hasError: state => state.errorString.length !== 0,
		shouldRedirect: state => state.redirect !== null,
		getAuthHeader: state => {
			return { headers: { "Authorization": localStorage.getItem('token') } };
		},
	},
	mutations: {
		logout( state ) {
			state.user = null;
			state.token = null;
			localStorage.removeItem('token');
		},
		login( state, data ) {
			state.user = data.user;
			state.token = data.token;
			localStorage.setItem('token', data.token);
		},
		setErrorString( state, error ) {
			state.errorString = error;
		},
		clearErrorString( state ) {
			state.errorString = '';
		},
		setRedirect( state, route ) {
			state.redirect = route;
		},
		clearRedirect( state ) {
			state.redirect = null;
		},
	},
	actions: {
		// Registration, Login //
		register(context, user) {
			axios.post( "api/users", user ).then( response => {
				context.commit('login', response.data);
				context.commit('clearErrorString');
			}).catch(error => {
				if (error.response) {
					if (error.response.status === 409)
						context.commit('setErrorString', "That user name is already taken.");
					return;
				}
				context.commit('setErrorString', "Sorry, your request failed. We will look into it.");
				context.commit('logout');
			});
		},
		login(context, user) {
			axios.post( "api/login", user ).then( response => {
				context.commit('login', response.data);
				context.commit('clearErrorString');
			}).catch(error => {
				if (error.response) {
					if (error.response.status === 403 || error.response.status === 400)
						context.commit('setErrorString', "Invalid login.");
					return;
				}
				context.commit('setErrorString', "Sorry, your request failed. We will look into it.");
				context.commit('logout');
			});
		},
		initialize(context) {
			let token = localStorage.getItem('token');
			if ( token ) {
				axios.get( 'api/me', context.getters.getAuthHeader ).then( response => {
					context.commit( 'login', { user: response.data.user, token: token } );
				} ).catch( error => {
					localStorage.removeItem('token');
					context.commit( 'logout' );
				} );
			}
		},
	},
});

export default store;
