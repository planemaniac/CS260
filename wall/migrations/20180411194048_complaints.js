exports.up = function(knex, Promise) {
	return Promise.all([
		knex.schema.createTable('complaints', function(table) {
			table.increments('id').primary();
			table.string('complaint');
			table.datetime('created');
			table.integer('user_id').unsigned().notNullable().references('id').inTable('users');
		}),
	]);
};

exports.down = function(knex, Promise) {
	return Promise.all([
		knex.schema.dropTable('complaints'),
	]);
};
