// Express Setup
const express = require('express');
const bodyParser = require("body-parser");

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static('dist'));

// Knex Setup
const env = process.env.NODE_ENV || 'development';
const config = require('./knexfile')[env];
const knex = require('knex')(config);

// bcrypt setup
let bcrypt = require('bcrypt');
const saltRounds = 10;

// jwt setup
const jwt = require("jsonwebtoken");
let jwtSecret = process.env.jwtSecret;
if ( jwtSecret === undefined ) {
	console.log( "You must define jwtSecret in environment" );
	knex.destroy();
	process.exit(1);
}

const verifyToken = (req, res, next) => {
	const token = req.headers['authorization'];
	if ( !token ) {
		return res.status(403).json( { error: "No Token provided" } );
	}

	jwt.verify( token, jwtSecret, function( err, decoded ) {
		if ( err ) {
			return res.status(500).json( { error: "Failed to decode token" } );
		}

		req.userId = decoded.id;
		next();
	} );
};

app.post( '/api/login', (req, res) => {
	if (!req.body.username || !req.body.password)
		return res.status(400).send();

	knex('users').where('username', req.body.username).first().then( user => {
		if (user === undefined) {
			res.status(403).send("Invalid credentials");
			throw new Error('abort');
		}
		return [bcrypt.compare(req.body.password, user.hash), user];
	}).spread( (result, user) => {
		if ( result ) {
			let token = jwt.sign( { id: user.id }, jwtSecret, {
				expiresIn: 86400, // 24 hours
			} );
			res.status(200).json( {
				user: {
					username: user.username, 
					name: user.name,
					id: user.id,
				},
				token: token,
			} );
		} else {
			res.status(403).send("Invalid credentials");
		}
		return;
	}).catch(error => {
		if (error.message !== 'abort') {
			console.log(error);
			res.status(500).json({ error });
		}
	});
});

app.post( '/api/users', (req, res) => {
	if (!req.body.password || !req.body.username || !req.body.name)
		return res.status(400).send();

	knex('users').where('username', req.body.username).first().then( user => {
		if (user !== undefined) {
			res.status(409).send("Username already exists");
			throw new Error('abort');
		}
		return bcrypt.hash(req.body.password, saltRounds);
	}).then(hash => {
		return knex('users').insert( {
			hash: hash,
			username: req.body.username,
			name: req.body.name
		} );
	}).then( ids => {
		return knex('users').where( 'id', ids[0] ).first().select( 'username', 'name', 'id' );
	}).then(user => {
		let token = jwt.sign( { id: user.id }, jwtSecret, {
			expiresIn: 86400, // 24 hours
		} );
		res.status(200).json( { user: user, token: token } );
		return;
	}).catch(error => {
		if (error.message !== 'abort') {
			console.log(error);
			res.status(500).json({ error });
		}
	});
});

app.get( '/api/me', verifyToken, (req, res) => {
	knex('users').where( 'id', req.userId )
		.select('username', 'name', 'id')
		.then( user => {
			res.status(200).json( { user: user } );
		} );
} );

app.get( '/api/complaints', verifyToken, (req, res) => {
	knex('users').join( 'complaints', 'users.id', 'complaints.user_id' )
		.orderBy('created', 'desc')
		.select('complaints.id as id', 'complaint', 'username', 'name', 'created')
		.then( complaints => {
			res.status(200).json( {
				complaints: complaints,
			} );
		} )
		.catch( error => {
			res.status(500).json( {
				error: error,
			} );
		} );
} );

app.post( '/api/complaints', verifyToken, (req, res) => {
	knex('users').where('id', req.userId).first().then( () => {
		return knex('complaints').insert( {
			user_id: req.userId,
			complaint: req.body.complaint,
			created: new Date(),
		} );
	} ).then( () => {
		res.status(200).json({});
	} ).catch( error => {
		res.status(500).json( {
			error: error,
		} );
	} );
} );

app.listen(3002, () => console.log('Server listening on port 3002!'));
